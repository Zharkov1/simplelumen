<?php
namespace  App\Repositories;

use App\Interfaces\UserInterface;
use App\Models\User;
use Illuminate\Support\Facades\DB;


class UserRepository implements UserInterface
{
    /**
     * @return array
     */
    public function getUsers(): array
    {
        //return app('db')->select("SELECT first_name, last_name, email, phone, password  FROM users");
        //Eloquent
        return User::all([
            'first_name',
            'last_name',
            'email',
            'phone',
            'password',
        ]);
    }

}
