<?php
use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Routing\Router;

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['prefix' => 'api/'], function($app){
   /* $app->post('car','UserController@store');
    $app->put('car/{id}','UserController@update');
    $app->delete('car/{id}','UserController@delete');*/
    $app->get('/','UserController@index');
    });
